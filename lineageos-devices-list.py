#!/usr/bin/env python

# This script reads the yaml files of the devices and produces a table
# with the information found in them. The path to the git repository of
# the wiki ("lineage_wiki") is expected as the only argument.

import os
from sys import argv
import re
from glob import glob
import yaml
from datetime import date



# Verification of the command line argument
if len(argv) != 2:
	raise RuntimeError("Path to the git repository of the Lineage Wiki expected as the only argument to this script")

if not os.path.isdir(argv[1]):
	raise RuntimeError('Invalid directory path "%s"' % argv[1])



# Constants
ROWS_BET_HEADERS = 25 # Number of rows to include between repeted headers
MAX_NAME_WIDTH = 40 # Limit on the width of the model name column
MAX_STORAGE_WIDTH = 25 # Limit on the width of the storage column

yes_symbol = "\033[0;32m\u2713\033[0m" # Green checkmark
no_symbol = "\033[0;31mX\033[0m" # Red X
partly_symbol = "\033[1;33m~\033[0m" # Yellow ~



# String length excluding the invisible ANSI color characters
def visual_len(s):
	return len(re.sub("\033\[.*?m", "", s))



devices = []
for f in glob(argv[1].rstrip("/") + "/_data/devices/*.yml"):
	with open(f) as fp:
		device = yaml.safe_load(fp)
	
	
	
	# Model name
	if len(device["name"]) > MAX_NAME_WIDTH:
		name = device["name"][:(MAX_NAME_WIDTH - 3)] + "..."
	else:
		name = device["name"]
	
	
	# Release date per submodel
	if type(device["release"]) is list:
		years = [] # Only the year of the releases
		for release in device["release"]:
			for submodelname in release:
				if type(release[submodelname]) is date:
					years.append(release[submodelname].year)
				elif type(release[submodelname]) is str:
					years.append(int(release[submodelname].split("-")[0]))
				elif type(release[submodelname]) is int:
					years.append(release[submodelname])
				else:
					#raise TypeError("Uncovered case for device release\n%s contains a release field of type %s:\n%s" % (f, type(release[submodelname]), str(release[submodelname])))
					print("Release of %s contains a field of type %s: %s" % (f, type(release[submodelname]), str(release[submodelname])))
		
		release = "%s-%s" % (min(years), max(years))
	
	elif type(device["release"]) is date:
		release = device["release"].strftime("%Y-%m-%d")
	
	elif type(device["release"]) is str:
		release = device["release"]
	
	elif type(device["release"]) is int:
		release = str(device["release"])
	
	else:
		#raise TypeError("Uncovered case for device release\n%s contains a release field of type %s:\n%s" % (f, type(device["release"]), str(device["release"])))
		print("Release of %s contains a field of type %s: %s" % (f, type(device["release"]), str(device["release"])))
	
	
	
	# Batteries per submodel
	# device["battery"] can be the string "None", a dictionary if
	# there are no submodels, or a list containing the submodels
	if device["battery"] is None or device["battery"] == "None":
		bat_cap = "N/A"
		bat_rem = no_symbol
	
	elif type(device["battery"]) is list:
		capacities = []
		removabilities = []
		
		for submodel in device["battery"]:
			for submodelname in submodel:
				capacities.append(submodel[submodelname]["capacity"])
				removabilities.append(submodel[submodelname]["removable"])
		
		bat_cap = "%s-%s" % (min(capacities), max(capacities))
		
		if all(removabilities):
			bat_rem = yes_symbol
		elif all([not x for x in removabilities]):
			bat_rem = no_symbol
		else: # If some batteries are removable and others aren't
			bat_rem = partly_symbol
	
	elif type(device["battery"]) is dict:
		bat_cap = str(device["battery"]["capacity"])
		bat_rem = yes_symbol if device["battery"]["removable"] else no_symbol
	
	else:
		#raise TypeError("Uncovered case for device battery\n%s contains a battery field of type %s:\n%s" % (f, type(device["battery"]), str(device["battery"])))
		print("Battery of %s contains a field of type %s: %s" % (f, type(device["battery"]), str(device["battery"])))
	
	
	
	# Storage per submodel
	if device["storage"] is None or device["storage"] == "None":
		storage = "N/A"
	elif type(device["storage"]) is str:
		if len(device["storage"]) > MAX_STORAGE_WIDTH:
			storage = device["storage"][:(MAX_STORAGE_WIDTH - 3)] + "..."
		else:
			storage = device["storage"]
	else:
		#raise TypeError("Uncovered case for device storage\n%s contains a storage field of type %s:\n%s" % (f, type(device["storage"]), str(device["storage"])))
		print("Storage of %s contains a field of type %s: %s" % (f, type(device["storage"]), str(device["storage"])))
	
	
	
	devices.append({
		"vendor": device["vendor"],
		"name": name,
		"release": release,
		"storage": storage,
		"bat_cap": bat_cap,
		"bat_rem": bat_rem,
		"gps": yes_symbol if "GPS" in device["peripherals"] else no_symbol,
		"fm_radio": yes_symbol if "FM radio" in device["peripherals"] else no_symbol
	})

if len(devices) == 0:
	raise RuntimeError("No yaml files found")



devices.sort(key = lambda device: device["release"])

header = {
	"vendor": "Vendor",
	"name": "Model",
	"release": "Release",
	"storage": "Storage",
	"bat_cap": "Bat. capacity (mAh)",
	"bat_rem": "Bat. removable",
	"gps": "GPS",
	"fm_radio": "FM Radio"
}

# Width of each column
width = {}
for k in devices[0]:
	width[k] = max([visual_len(dev[k]) for dev in devices] + [visual_len(header[k])])

# Printing of the table
for (i, dev) in enumerate(devices):
	# To account for the invisible ANSI color characters
	visual_width = lambda key: width[key] + len(dev[key]) - visual_len(dev[key])
	
	# If it's time to repeat the header to identify the columns
	if i % ROWS_BET_HEADERS == 0:
		print("%*s | %*s | %*s | %*s | %*s | %*s | %*s | %*s" % (
			width["vendor"], header["vendor"],
			width["name"], header["name"],
			width["release"], header["release"],
			width["storage"], header["storage"],
			width["bat_cap"], header["bat_cap"],
			width["bat_rem"], header["bat_rem"],
			width["gps"], header["gps"],
			width["fm_radio"], header["fm_radio"]
		))
		print("-" * (sum([width[k] for k in width]) + (len(width) - 1) * 3)) # * 3 is for " | "
	
	
	print("%*s   %*s   %*s   %*s   %*s   %*s   %*s   %*s" % (
		visual_width("vendor"), dev["vendor"],
		visual_width("name"), dev["name"],
		visual_width("release"), dev["release"],
		visual_width("storage"), dev["storage"],
		visual_width("bat_cap"), dev["bat_cap"],
		visual_width("bat_rem"), dev["bat_rem"],
		visual_width("gps"), dev["gps"],
		visual_width("fm_radio"), dev["fm_radio"]
	))

