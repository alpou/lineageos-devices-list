# List of the devices presented in LineageOS's wiki

Simple Python script that prints a complete list of all the devices
listed in [LineageOS's wiki](https://wiki.lineageos.org/devices/).


## Description

I thought I had already seen a similar list on their wiki, but I could
not find it. Still, I wanted to be able to list all the devices, sort
them by their release date, and compare some of their features. So I
wrote this short script, then I thought it could benefit others and
published it here.


## Dependencies

To use this script, you will need the following:
1. A posix shell:  
This is standard on all Unix-like operating systems. On Windows, you
can install [Git-Bash](https://git-scm.com/download/win).

2. `git`:  
This is provided by the `git` package on most Linux or BSD
distributions. On Windows, you can install
[Git-Bash](https://git-scm.com/download/win).

3. `python`:  
This is provided by the `python` or the `python3` package on most Linux
or BSD distributions. On Windows, you can install Python from
[their website](https://www.python.org/downloads/windows/).

4. PyYAML:  
    This library parses the yaml files making up LineageOS's wiki. It is
    provided by the following packages:
    
    * `python-pyaml` on Arch-based distributions;
    * `python-yaml` on Debian-based distributions;
    * `python-pyyaml` on RedHat/Fedora-based distributions;
    * `dev-python/pyyaml` on Gentoo-based distributions;
    * `py-yaml` on OpenBSD.
    
    Otherwise, it can be installed with `pip`:
    
        $ pip install PyYAML


## Usage

First clone this repository:

    $ git clone https://gitlab.com/alpou/lineageos-devices-list.git

Then run the following script:

    $ ./lineageos-devices-list


## More information

This repository contains two scripts: a shell script
(`lineageos-devices-list`) and a python script
(`lineageos-devices-list.py`).

The shell script is intended to be called directly, as it retrieves the
files of the wiki (using `git`) and then calls the python script.

The python script is called with the path to the wiki's repo, and then
goes through all the files matching `_data/devices/*.yml` in the repo.
The content of the files is stored in a list and displayed as a table at
the end of the script. The information included in the final table is
the vendor and model name, the release date, and information about the
storage, the battery, and other peripherals. The script can be easily
modified to add or remove columns in the table.


## Licensing

The content of this repository is provided under the GPLv3 license. See
the included LICENSE file for more information.
